import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from './custom_imports/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { routing } from './app.routing';

const modules = [
  BrowserModule,
  HttpModule,
  HttpClientModule,
  routing,
  ReactiveFormsModule,
  FormlyModule.forRoot(),
  FormlyMaterialModule,
  BrowserAnimationsModule,
  MaterialModule
];

export const Modules = [ modules ];
