import { Injectable } from '@angular/core';
import { Observable, of, forkJoin } from 'rxjs';

import { FormService } from '../services/form.service';
import { FieldGroupService } from '../services/fieldgroup.service';

import { Industry } from '../models/industry';
import { ICheckboxItem} from '../contracts/icheckboxItem';
import { IFormComponent } from '../contracts/iform-component';

@Injectable({
  providedIn: 'root'
})
export class IndustryService {

  private checkboxItems: ICheckboxItem[] = [];

  constructor(private formService: FormService,
              private fieldGroupService: FieldGroupService) {
    this.createCheckboxItems();
  }

  buildIndustries(): Observable<IFormComponent> {
    let result: IFormComponent;
    result = {
      model: this.getNewIndustriesModel(),
      fields: this.getIndustryFields()
    };
    return of(result);
  }

  private getNewIndustriesModel() {
    return new Industry();
  }

  private createCheckboxItems() {
    const checkboxClassName = 'flex';
    this.checkboxItems = [];
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'transporter', label: 'Transporter', className: checkboxClassName, groupIndex: 1 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'distributor', label: 'Distributor', className: checkboxClassName, groupIndex: 1 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'manufacturer', label: 'Manufacturer', className: checkboxClassName, groupIndex: 1 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'reseller', label: 'Reseller', className: checkboxClassName, groupIndex: 2 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'farmer', label: 'Farmer', className: checkboxClassName, groupIndex: 2 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'earthmoving', label: 'Earthmoving', className: checkboxClassName, groupIndex: 2 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'heavyhaulage', label: 'Heavy Haulage', className: checkboxClassName, groupIndex: 3 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'crossborder', label: 'Cross Border', className: checkboxClassName, groupIndex: 3 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'otherIndustry', label: 'Other', className: checkboxClassName, groupIndex: 3 }) );
  }

  private getIndustryFields() {
    return this.formService.createFormlyCheckboxFields(this.checkboxItems);
  }
}
