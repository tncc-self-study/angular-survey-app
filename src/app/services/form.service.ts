import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';

import { FieldGroupService } from './fieldgroup.service';

import { ICheckboxItem } from '../contracts/icheckboxItem';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(private fieldGroupService: FieldGroupService) { }

  // get all values of the formGroup, loop over them
  // then mark each field as touched
  markFormGroupTouched(formGroup: FormGroup) {
    Object.values(formGroup.controls).forEach(control => {
      control.markAsTouched();
    });
  }

  createCheckboxItem(chkboxItem: ICheckboxItem): { key: string, label: string, className: string, groupIndex: number } {
    return { key: chkboxItem.key, label: chkboxItem.label, className: chkboxItem.className, groupIndex: chkboxItem.groupIndex };
  }

  createFormlyCheckbox(checkboxItem: ICheckboxItem) {
    const item: FormlyFieldConfig = {
      key: checkboxItem.key,
      type: 'checkbox',
      className: checkboxItem.className,
      templateOptions: {
        label: checkboxItem.label,
        attributes: {
          data_type: 'checkbox'
        }
      }
    };
    return item;
  }

  createFormlyCheckboxFields(checkboxItems: ICheckboxItem[]) {
    const fields: FormlyFieldConfig[] = [];
    checkboxItems.forEach(checkboxItem => {
      const item = this.createFormlyCheckbox(checkboxItem);
      fields.push(item);
    });
    return fields;
  }

  createGroupedFormlyCheckboxFields(checkboxItems: ICheckboxItem[]) {
    const fields: FormlyFieldConfig[] = [];
    let groupedFields: FormlyFieldConfig;
    let oldGroupIndex = 0;
    for (let i = 0; i < checkboxItems.length; i++) {
      const groupIndex = checkboxItems[i].groupIndex;
      if (oldGroupIndex === 0 || groupIndex > oldGroupIndex) {
        if (oldGroupIndex > 0) {
          fields.push(groupedFields);
        }
        groupedFields = this.fieldGroupService.getCheckboxGroup();
        oldGroupIndex = groupIndex;
      }

      const checkboxItem = checkboxItems[i];
      const item = this.createFormlyCheckbox(checkboxItem);
      groupedFields.fieldGroup.push(item);

      if (checkboxItems.length - 1 === i) {
        fields.push(groupedFields);
      }
    }
    return fields;
  }
}
