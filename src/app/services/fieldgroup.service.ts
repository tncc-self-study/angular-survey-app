import { Injectable } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Injectable({
  providedIn: 'root'
})
export class FieldGroupService {

  constructor() { }

  getCheckboxGroup(): FormlyFieldConfig {
    const item: FormlyFieldConfig = {
      className: 'flex',
      templateOptions: {
          attributes: {
            layout: 'row',
            layout_sm: 'column'
          }
        },
      fieldGroup: []
    };

    return item;
  }
}
