import { Injectable } from '@angular/core';

import { DataService } from '../services/data.service';

import { INampo } from '../contracts/inampo';

@Injectable({
  providedIn: 'root'
})
export class NampoService {

  saveurl = 'survey/save';

  constructor(private dataService: DataService) { }

  save(model: any) {
    return this.dataService.add<INampo>(this.saveurl, model);
  }
}
