import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthService } from './auth.service';
import { ToasterService } from './toaster.service';

import { Configuration } from '../app.constants';

@Injectable()
export class DataService {

    private serverApiUrl: string;

    constructor(private http: HttpClient,
                _configuration: Configuration) {
        this.serverApiUrl = _configuration.ServerWithApiUrl;
    }

    public getAll<T>(urlPartial: string): Observable<T> {
      const actionUrl = this.serverApiUrl + urlPartial;
      return this.http.get<T>(actionUrl);
    }

    public getSingle<T>(urlPartial: string, id: number): Observable<T> {
      const actionUrl = this.serverApiUrl + urlPartial + id;
      return this.http.get<T>(actionUrl);
    }

    public add<T>(urlPartial: string, itemToAdd: any): Observable<T> {
      const toAdd = JSON.stringify(itemToAdd);
      const actionUrl = this.serverApiUrl + urlPartial;
      return this.http.post<T>(actionUrl, toAdd);
    }

    public update<T>(urlPartial: string, id: number, itemToUpdate: any): Observable<T> {
      const actionUrl = this.serverApiUrl + urlPartial + id;
      return this.http
                 .put<T>(actionUrl, JSON.stringify(itemToUpdate));
    }

    public delete<T>(urlPartial: string, id: number): Observable<T> {
      const actionUrl = this.serverApiUrl + urlPartial + id;
      return this.http.delete<T>(actionUrl);
    }
}


@Injectable()
export class JsonContentTypeInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService,
              private toasterService: ToasterService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!req.headers.has('Content-Type')) {
      req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
    }
    // This guard clause below is very important to ensure that authentication can intially occur.
    if (req.url.indexOf('/login') > 0) {
      return next.handle(req);
    }

    if (!this.authService.isAuthenticated()) {
      this.toasterService.showWarnToast('HHHhhhhhhmmmmm..... you\'ll need to login! Data cannot be retrieved or sent to the database.');
    } else {
      const authToken = this.authService.getSavedToken();
      req = req.clone({
        headers: req.headers
                    .set('Accept', 'application/json')
                    .set('Authorization', 'Bearer ' + authToken)
        });
      return next.handle(req);
    }

  }
}
