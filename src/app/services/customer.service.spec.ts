import { TestBed, inject } from '@angular/core/testing';

import { Services\customerService } from './services\customer.service';

describe('Services\customerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Services\customerService]
    });
  });

  it('should be created', inject([Services\customerService], (service: Services\customerService) => {
    expect(service).toBeTruthy();
  }));
});
