import { Injectable } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';

import { ValidatorsService } from '../services/validators.service';

import { Customer } from '../models/customer';
import { ICustomer } from '../contracts/icustomer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private validatorsService: ValidatorsService) {}

  getNewCustomerModel() {
    return new Customer();
  }

  getCustomerFields(areas: any[]) {
    const fields: FormlyFieldConfig[] = [
      {
        key: 'customerName',
        type: 'input',
        templateOptions: {
          type: 'text',
          label: 'Customer Name',
          placeholder: 'Customer Name',
          required: true
        },
        validators: {
          validateStringMinLength: {
            expression: (c) => this.validatorsService.validateStringMinLength(3, c.value),
            message: (error, field: FormlyFieldConfig) => `A customer name is required`
          }
        }
      },
      {
        key: 'isCurrentCustomer',
        type: 'checkbox',
        templateOptions: {
          label: 'Current Customer'
        }
      },
      {
        key: 'area',
        type: 'select',
        templateOptions: {
          label: 'Areas',
          options: areas,
          required: true
        },
        validators: {
          validateStringMinLength: {
            expression: (c) => this.validatorsService.validateStringMinLength(3, c.value),
            message: (error, field: FormlyFieldConfig) => `An area must be selected`
          }
        }
      },
      {
        key: 'town',
        type: 'input',
        templateOptions: {
          type: 'text',
          label: 'City/Town',
          placeholder: 'City/Town',
          required: true
        },
        validators: {
          validateStringMinLength: {
            expression: (c) => this.validatorsService.validateStringMinLength(3, c.value),
            message: (error, field: FormlyFieldConfig) => `City/Town name is required`
          }
        }
      },
      {
        key: 'telephone',
        type: 'input',
        templateOptions: {
          type: 'text',
          label: 'Telephone',
          placeholder: 'Telephone',
          required: true
        },
        validators: {
          validateStringMinLength: {
            expression: (c) => this.validatorsService.validateStringMinLength(3, c.value),
            message: (error, field: FormlyFieldConfig) => `A telephone number is required`
          }
        }
      },
      {
        key: 'fax',
        type: 'input',
        templateOptions: {
          type: 'text',
          label: 'Fax',
          placeholder: 'Fax'
        }
      },
      {
        key: 'mobile',
        type: 'input',
        templateOptions: {
          type: 'text',
          label: 'Cellphone',
          placeholder: 'Cellphone'
        }
      },
      {
        key: 'email',
        type: 'input',
        templateOptions: {
          type: 'email',
          label: 'Email',
          placeholder: 'Email',
          required: true
        },
        validators: {
          validateStringMinLength: {
            expression: (c) => this.validatorsService.validateStringMinLength(3, c.value),
            message: (error, field: FormlyFieldConfig) => `An email address is required`
          },
          validateEmail: {
            expression: (c) => this.validatorsService.validateEmail(c.value),
            message: (error, field: FormlyFieldConfig) => `A valid email address is required`
          }
        }
      }
    ];

    return fields;
  }
}
