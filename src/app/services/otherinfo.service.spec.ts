import { TestBed, inject } from '@angular/core/testing';

import { OtherInfoService } from './otherinfo.service';

describe('OtherinfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OtherInfoService]
    });
  });

  it('should be created', inject([OtherInfoService], (service: OtherInfoService) => {
    expect(service).toBeTruthy();
  }));
});
