import { TestBed, inject } from '@angular/core/testing';

import { NampoService } from './nampo.service';

describe('NampoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NampoService]
    });
  });

  it('should be created', inject([NampoService], (service: NampoService) => {
    expect(service).toBeTruthy();
  }));
});
