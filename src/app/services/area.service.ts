import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DataService } from '../services/data.service';

import { IArea } from '../contracts/iarea';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  areasurl = 'areas';

  constructor(private dataService: DataService) {}

  getAllAreas(): Observable<IArea[]> {
    return this.dataService.getAll<IArea[]>(this.areasurl);
  }

  getAreas() {
    return  [
      {label: 'Eastern Cape',  value: 'Eastern Cape'},
      {label: 'Free State',  value: 'Free State'},
      {label: 'Gauteng',   value: 'Gauteng'},
      {label: 'KwaZulu-Natal', value: 'KwaZulu-Natal'},
      {label: 'Limpopo', value: 'Limpopo'},
      {label: 'Mpumalanga', value: 'Mpumalanga'},
      {label: 'Northern Cape', value: 'Northern Cape'},
      {label: 'North West', value: 'North West'},
      {label: 'Western Cape', value: 'Western Cape'}
    ];
  }

}
