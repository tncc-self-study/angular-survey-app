import { TestBed, inject } from '@angular/core/testing';

import { FieldGroupService } from './fieldgroup.service';

describe('FieldGroupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FieldGroupService]
    });
  });

  it('should be created', inject([FieldGroupService], (service: FieldGroupService) => {
    expect(service).toBeTruthy();
  }));
});
