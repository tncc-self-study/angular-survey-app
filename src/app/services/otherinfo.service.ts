import { Injectable } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';

import { ValidatorsService } from '../services/validators.service';

import { OtherInfo } from '../models/otherinfo';

@Injectable({
  providedIn: 'root'
})
export class OtherInfoService {

  constructor(private validatorsService: ValidatorsService) { }

  getNewOtherInfoModel() {
    return new OtherInfo();
  }

  getOtherInfoFields(reps: any[]) {
    const fields: FormlyFieldConfig[] = [
      {
        key: 'comments',
        type: 'textarea',
        templateOptions: {
          type: 'text',
          label: 'Comments',
          placeholder: 'Comments'
        }
      },
      {
        key: 'isUrgent',
        type: 'checkbox',
        templateOptions: {
          label: 'Urgent'
        }
      },
      {
        key: 'rep',
        type: 'select',
        templateOptions: {
          label: 'Completed by',
          required: true,
          options: reps
        },
        validators: {
          validateStringMinLength: {
            expression: (c) => this.validatorsService.validateStringMinLength(3, c.value),
            message: (error, field: FormlyFieldConfig) => `Please select the name of the person who conducted the survey, it is required.`
          }
        }
      }
    ];

    return fields;
  }
}
