import { TestBed, inject } from '@angular/core/testing';

import { SalesRepsService } from './salesreps.service';

describe('SalesrepsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SalesRepsService]
    });
  });

  it('should be created', inject([SalesRepsService], (service: SalesRepsService) => {
    expect(service).toBeTruthy();
  }));
});
