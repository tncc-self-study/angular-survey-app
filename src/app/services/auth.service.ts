import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Configuration } from '../app.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private tokenUrl = 'survey/login';
  public tokenKey = 'token';

  private serverApiUrl: string;

  constructor(private http: HttpClient,
              _configuration: Configuration) {
    this.serverApiUrl = _configuration.ServerWithApiUrl;
  }

  getSavedToken(): string {
    return localStorage.getItem(this.tokenKey);
  }

  isAuthenticated(): boolean {
    return localStorage.getItem(this.tokenKey) != null;
  }

  login(username: string, password: string): Observable<string> {
    const actionUrl = this.serverApiUrl + this.tokenUrl;
    const body = JSON.stringify({ username: username, password: password });
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const result = this.http.post<string>(actionUrl, body, { headers: headers });

    return result;
  }

  logOut() {
    localStorage.removeItem(this.tokenKey);
  }
}
