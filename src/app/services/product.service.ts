import { Injectable } from '@angular/core';
import { FormlyFieldConfig, FormlyField } from '@ngx-formly/core';
import { Observable, of, forkJoin } from 'rxjs';

import { FormService } from '../services/form.service';

import { Product } from '../models/product';
import { ICheckboxItem} from '../contracts/icheckboxItem';
import { FormlyGroup } from '@ngx-formly/core/lib/components/formly.group';
import { IProduct } from '../contracts/iproduct';
import { IFormComponent } from '../contracts/iform-component';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private checkboxItems: ICheckboxItem[] = [];

  constructor(private formService: FormService) {
    this.createCheckboxItems();
  }

  buildProducts(): Observable<IFormComponent> {
    let result: IFormComponent;
    result = {
      model: this.getNewProductsModel(),
      fields: this.getProductFields()
    };
    return of(result);
  }

  private getNewProductsModel() {
    return new Product();
  }

  private createCheckboxItems() {
    const checkboxClassName = 'flex-1';
    this.checkboxItems = [];
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'mercedes', label: 'Mercedes', className: checkboxClassName, groupIndex: 1 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'freightLiner', label: 'FreightLiner', className: checkboxClassName, groupIndex: 1 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'iveco', label: 'Iveco', className: checkboxClassName, groupIndex: 1 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'truckbodyparts', label: 'Truck Body Parts', className: checkboxClassName, groupIndex: 2 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'man', label: 'MAN', className: checkboxClassName, groupIndex: 2 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'volvo', label: 'Volvo', className: checkboxClassName, groupIndex: 2 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'international', label: 'International', className: checkboxClassName, groupIndex: 3 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'tankers', label: 'Tankers', className: checkboxClassName, groupIndex: 3 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'jonesco', label: 'Jonesco', className: checkboxClassName, groupIndex: 3 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'truckaccessories', label: 'Truck Accessories', className: checkboxClassName, groupIndex: 4 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'scania', label: 'Scania', className: checkboxClassName, groupIndex: 4 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'renault', label: 'Renault', className: checkboxClassName, groupIndex: 4 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'trailers', label: 'Trailers', className: checkboxClassName, groupIndex: 5 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'fueldefend', label: 'Fuel Defend', className: checkboxClassName, groupIndex: 5 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'daf', label: 'DAF', className: checkboxClassName, groupIndex: 5 }) );
    this.checkboxItems.push( this.formService.createCheckboxItem({ key: 'otherProduct', label: 'Other', className: checkboxClassName, groupIndex: 6 }) );
  }

  private getProductFields() {
    return this.formService.createFormlyCheckboxFields(this.checkboxItems);
  }
}
