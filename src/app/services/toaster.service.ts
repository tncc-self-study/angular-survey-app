import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor(private snackBar: MatSnackBar) {}

  private createConfig(panelClass: string) {
    const config = new MatSnackBarConfig();
    config.verticalPosition = 'bottom';
    config.horizontalPosition = 'left';
    config.duration = 5000;
    config.panelClass = panelClass;
    return config;
  }

  showPrimaryToast(msg: string) {
    const config = this.createConfig('primary-snackbar');
    this.snackBar.open(msg, null, config);
  }

  showInfoToast(msg: string) {
    const config = this.createConfig('info-snackbar');
    this.snackBar.open(msg, null, config);
  }

  showWarnToast(msg: string) {
    const config = this.createConfig('warn-snackbar');
    this.snackBar.open(msg, null, config);
  }
}
