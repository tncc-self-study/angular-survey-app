import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';

import { DataService } from '../services/data.service';

import { ISalesRep } from '../contracts/isalesrep';

@Injectable({
  providedIn: 'root'
})
export class SalesRepsService {

  salesrepurl = 'salesreps';

  constructor(private dataService: DataService) { }

  getAllSalesReps(): Observable<ISalesRep[]> {
    return this.dataService.getAll<ISalesRep[]>(this.salesrepurl);
  }

  getSalesReps() {
    return  [
      {label: 'Jimmy',  value: 'Jimmy'},
      {label: 'Gary',  value: 'Gary'},
      {label: 'Corne',   value: 'Corne'},
      {label: 'Melissa', value: 'Melissa'},
      {label: 'Paul', value: 'Paul'},
      {label: 'Lance', value: 'Lance'},
      {label: 'Assistant', value: 'Assistant'}
    ];
  }
}
