import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidatorsService {

  constructor() { }

  validateMustBeChecked(value?: boolean) {
    return value !== null && value === true;
  }

  validateStringMaxLength(max: number, value?: string) {
      return value !== null && value.length <= max;
  }

  validateStringMinLength(min: number, value?: string) {
    return value !== null && value.length >= min;
  }

  validateStringRange(min: number, max: number, value?: string) {
    return this.validateStringMinLength(min, value) && this.validateStringMaxLength(max, value);
  }

  validateNumericRange(min: number, max: number, value?: number) {
    return value !== null && (value >= min && value <= max);
  }

  validateEmail(value?: string) {
    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    return value !== null && EMAIL_REGEXP.test(value);
  }

  validationMessages() {
    const messages = {
      required: 'This field is required',
      email: 'This email address is invalid',
      validateEmail: 'This email address is invalid',
      validateMustBeChecked: 'This option must be selected',
      validateStringMaxLength: 'This string exceeds the maximum length',
      validateStringMinLength: 'This string is too short',
      validateStringRange: 'This string is not within the minimum and maximum lengths',
      validateNumericRange: 'This number does not fall within the allowed minimum and maximum value',
      validateNumericMax: 'This number is greater than the allowed maximum',
      invalid_characters: (matches: any[]) => {

        let matchedCharacters = matches;

        matchedCharacters = matchedCharacters.reduce((characterString, character, index) => {
          let string = characterString;
          string += character;

          if (matchedCharacters.length !== index + 1) {
            string += ', ';
          }

          return string;
        }, '');

        return `These characters are not allowed: ${matchedCharacters}`;
      },
    };

    return messages;
  }

  validateForm(formToValidate: FormGroup, formErrors: any, checkDirty?: boolean) {
    const form = formToValidate;

    for (const field in formErrors) {
      if (field) {
        formErrors[field] = '';
        const control = form.get(field);

        const messages = this.validationMessages();
        if (control && !control.valid) {
          if (!checkDirty || (control.dirty || control.touched)) {
            for (const key in control.errors) {
              if (key && key !== 'invalid_characters') {
                formErrors[field] = formErrors[field] || messages[key];
              } else {
                formErrors[field] = formErrors[field] || messages[key](control.errors[key]);
              }
            }
          }
        }
      }
    }

    return formErrors;
  }
}
