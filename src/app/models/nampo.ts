import { INampo } from '../contracts/inampo';

export class Nampo implements INampo {
  // Customer options
  email?: string;
  mobile?: string;
  fax?: string;
  telephone?: string;
  town?: string;
  area?: string;
  areas?: any[];
  isCurrentCustomer?: boolean;
  customerName?: string;
  // Products of interest
  mercedes?: boolean;
  freightLiner?: boolean;
  iveco?: boolean;
  truckbodyparts?: boolean;
  man?: boolean;
  volvo?: boolean;
  international?: boolean;
  tankers?: boolean;
  jonesco?: boolean;
  truckaccessories?: boolean;
  scania?: boolean;
  renault?: boolean;
  trailers?: boolean;
  fueldefend?: boolean;
  daf?: boolean;
  otherProduct?: boolean;
  // Types of industry
  transporter?: boolean;
  distributor?: boolean;
  manufacturer?: boolean;
  reseller?: boolean;
  farmer?: boolean;
  earthmoving?: boolean;
  heavyhaulage?: boolean;
  crossborder?: boolean;
  otherIndustry?: boolean;
  // Other information
  comments?: string;
  isUrgent?: boolean;
  rep?: string;
  reps?: any[];

  constructor() {}
}
