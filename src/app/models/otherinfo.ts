import { IOtherInfo } from '../contracts/iotherinfo';
import { ICollection } from '../contracts/icollection';

export class OtherInfo implements IOtherInfo, ICollection {
  comments?: string;
  isUrgent?: boolean;
  rep?: string;
  reps?: any[];

  defaultOptions = {
    comments: '',
    isUrgent: false,
    rep: ''
  };

  constructor() {
      this.comments = this.defaultOptions.comments;
      this.isUrgent = this.defaultOptions.isUrgent;
      this.rep = this.defaultOptions.rep;
  }

  setCollection(source?: any[]) {
    this.reps = source;
  }
}
