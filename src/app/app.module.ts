import { NgModule } from '@angular/core';
import { Declarations } from './app.declarations';
import { Modules } from './app.modules';
import { Providers } from './app.providers';

import { AppComponent } from './app.component';
import { ErrorComponent } from './components/error/error.component';

@NgModule({
  declarations: [ Declarations, ErrorComponent ],
  imports: [ Modules ],
  providers: [ Providers ],
  bootstrap: [AppComponent]
})
export class AppModule { }
