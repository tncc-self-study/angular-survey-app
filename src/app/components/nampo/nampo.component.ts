import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import * as $ from 'jquery';

import { CustomerService } from '../../services/customer.service';
import { ProductService } from '../../services/product.service';
import { IndustryService } from '../../services/industry.service';
import { OtherInfoService } from '../../services/otherinfo.service';
import { SalesRepsService } from '../../services/salesreps.service';
import { AreaService } from '../../services/area.service';
import { NampoService } from '../../services/nampo.service';
import { ToasterService } from '../../services/toaster.service';
import { AuthService } from '../../services/auth.service';

import { INampo } from '../../contracts/inampo';
import { Nampo } from '../../models/nampo';
import { IFormComponent } from '../../contracts/iform-component';

@Component({
  selector: 'app-nampo',
  templateUrl: './nampo.component.html',
  styleUrls: ['./nampo.component.css']
})
export class NampoComponent implements OnInit {

  form = new FormGroup({});
  options: FormlyFormOptions = {
    formState: {
      disabled: true,
    },
  };
  model: Nampo = {};
  customerFields: FormlyFieldConfig[] = [];
  productsFields: FormlyFieldConfig[] = [];
  industriesFields: FormlyFieldConfig[] = [];
  otherInfoFields: FormlyFieldConfig[] = [];

  constructor(private customerService: CustomerService,
              private productService: ProductService,
              private industryService: IndustryService,
              private otherInfoService: OtherInfoService,
              private salesRepsService: SalesRepsService,
              private areaService: AreaService,
              private nampoService: NampoService,
              private toasterService: ToasterService,
              private authService: AuthService,
              private router: Router) {
              }

  ngOnInit() {
    if (!this.authService.isAuthenticated()) {
      const dummyUser = 'I_AM_ROOT'; // !!!! NOT FOR PRODUCTION USE - RECEIVE THIS FROM CLIENT INPUT
      const dummyPass = 'I_AM_ROOT'; // !!!! NOT FOR PRODUCTION USE - RECEIVE THIS FROM CLIENT INPUT
      this.toasterService.showInfoToast('HHHHhhhhhmmmmm...... faked the login :-)');
      this.authService.login(dummyUser, dummyPass)
          .subscribe((auth: any) => {
            localStorage.setItem(this.authService.tokenKey, auth.auth_token);
            this.buildComponents();
          },
          (error) => {
            this.toasterService.showWarnToast(error.message);
            this.gotoAuthFailed();
          }
        );
    } else {
      this.buildComponents();
    }
  }

  buildComponents() {
    this.model = new Nampo();
    this.buildCustomer();
    this.buildProducts();
    this.buildIndustries();
    this.buildOtherInfo();

    this.setLayout();
  }

  buildCustomer() {
    this.areaService.getAllAreas()
        .subscribe((reps) => {
          const fields = this.customerService.getCustomerFields(reps);
          const model = this.customerService.getNewCustomerModel();
          model.setCollection(reps);

          this.customerFields = fields;

          this.model.email = model.email;
          this.model.mobile = model.mobile;
          this.model.fax = model.fax;
          this.model.telephone = model.telephone;
          this.model.town = model.town;
          this.model.area = model.area;
          this.model.isCurrentCustomer = model.isCurrentCustomer;
          this.model.customerName = model.customerName;

          this.model.areas = model.areas;
        });
  }

  buildProducts() {
    this.productService.buildProducts()
        .subscribe((result: IFormComponent) => {
          this.productsFields = result.fields;

          this.model.mercedes = result.model.mercedes;
          this.model.freightLiner = result.model.freightLiner;
          this.model.iveco = result.model.iveco;
          this.model.truckbodyparts = result.model.truckbodyparts;
          this.model.man = result.model.man;
          this.model.volvo = result.model.volvo;
          this.model.international = result.model.international;
          this.model.tankers = result.model.tankers;
          this.model.jonesco = result.model.jonesco;
          this.model.truckaccessories = result.model.truckaccessories;
          this.model.scania = result.model.scania;
          this.model.renault = result.model.renault;
          this.model.trailers = result.model.trailers;
          this.model.fueldefend = result.model.fueldefend;
          this.model.daf = result.model.daf;
          this.model.otherProduct = result.model.otherProduct;
        });
  }

  buildIndustries() {
    this.industryService.buildIndustries()
        .subscribe((result: IFormComponent) => {
          this.industriesFields = result.fields;

          this.model.transporter = result.model.transporter;
          this.model.distributor = result.model.distributor;
          this.model.manufacturer = result.model.manufacturer;
          this.model.reseller = result.model.reseller;
          this.model.farmer = result.model.farmer;
          this.model.earthmoving = result.model.earthmoving;
          this.model.heavyhaulage = result.model.heavyhaulage;
          this.model.crossborder = result.model.crossborder;
          this.model.otherIndustry = result.model.otherIndustry;
        });
  }

  buildOtherInfo() {
    this.salesRepsService.getAllSalesReps()
        .subscribe((areas: any[]) => {
          const fields = this.otherInfoService.getOtherInfoFields(areas);
          const model = this.otherInfoService.getNewOtherInfoModel();
          model.setCollection(areas);

          this.otherInfoFields = fields;

          this.model.comments = model.comments;
          this.model.isUrgent = model.isUrgent;
          this.model.rep = model.rep;

          this.model.reps = model.reps;
        });
  }

  setLayout() {
    $(document).ready(function() {
      $('mat-checkbox').parentsUntil('mat-formfield').attr('style', '');
    });
  }

  gotoAuthFailed() {
    this.router.navigate(['/auth-failed']);
  }

  onSubmit(model: INampo) {
    if (this.form.valid) {
      this.nampoService.save(model)
          .subscribe((response) => {
            this.toasterService.showPrimaryToast('Survey saved successfully');
            setTimeout(() => {
              this.form.reset();
              window.scrollTo(0, 0);
            }, 2000);
          },
          (error) => {
            this.toasterService.showWarnToast('Survey saved failed');
            console.log(error);
          });
    }
  }
}
