import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';

import { FormService } from '../../services/form.service';
import { ValidatorsService } from '../../services/validators.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  form = new FormGroup({});

  options: FormlyFormOptions = {
    formState: {
      disabled: true,
    },
  };

  model = {
    tagName: '',
    color: 'powder-blue', // will default to this value
    quantity: 1,
    size: 'M',
    terms: false
  };

  fields: FormlyFieldConfig[] = [
    {
      key: 'tagName',
      type: 'input', // input type
      templateOptions: {
        type: 'text',
        label: 'Tag name for your outfit',
        placeholder: 'tag name'
      },
      validators: {
        validateStringMinLength: {
          expression: (c) => this.validatorsService.validateStringMinLength(25, c.value),
          message: (error, field: FormlyFieldConfig) => `Supplied input is less than require length (25)`
        }
      }
    },
    {
      key: 'color',
      type: 'select',
      templateOptions: {
        label: 'Outfit color',
        options: [
          { label: 'Powder blue', value: 'powder-blue' },
          { label: 'Orange crush', value: 'orange-crush' },
          { label: 'Purple haze', value: 'purple-haze' }
        ]
      }
    },
    {
      key: 'quantity',
      type: 'input',
      templateOptions: {
        type: 'number',
        label: 'How many outfits?',
        placeholder: 'quantity',
        required: true
      },
      validators: {
        validateNumericRange: {
          expression: (c) => this.validatorsService.validateNumericRange(1, 99, c.value),
          message: `The supplied qty must be between 1 and 99`
        }
      }
    },
    {
      key: 'size',
      type: 'select',
      defaultValue: 'M',
      templateOptions: {
        label: 'Size',
        options: [
          { label: 'Small', value: 'S' },
          { label: 'Medium', value: 'M' },
          { label: 'Large', value: 'L' }
        ]
      }
    },
    {
      key: 'terms',
      type: 'checkbox',
      templateOptions: {
        label: 'You accept our terms and conditions',
        required: true
      },
      validators: {
        mustBeChecked: {
          expression: (c) => this.validatorsService.validateMustBeChecked(c.value),
          message: `Please accept the terms`
        }
      }
    }
  ];

  errors = {
    tagName: '',
    color: '',
    quantity: '',
    size: '',
    terms: ''
  };

  onSubmit(orderInfo) {
    this.formService.markFormGroupTouched(this.form);

    if (this.form.valid) {
      console.log('Form is valid. ' + JSON.stringify(orderInfo));
      setTimeout(() => this.form.reset(), 5000);
    } else {
      console.log('Form is invalid.');
      this.errors = this.validatorsService.validateForm(this.form, this.errors, false);
    }
  }

  constructor(private formService: FormService,
              private validatorsService: ValidatorsService) { }

  ngOnInit() {
    // on each value change we call the validateForm
    this.form.valueChanges.subscribe((data) => {
      this.errors = this.validatorsService.validateForm(this.form, this.errors, true);
      this.toggleDisabled();
    });
  }

  toggleDisabled() {
    this.options.formState.disabled = this.form.valid;
  }
}
