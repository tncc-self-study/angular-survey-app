import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { ToasterService } from '../../services/toaster.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class NavMenuComponent {
  constructor(private toasterService: ToasterService,
              private authService: AuthService,
              private router: Router) { }

  onLogout() {
    this.authService.logOut();
    this.toasterService.showWarnToast('You are now logged out!');
    this.router.navigate(['/home']);
  }
}
