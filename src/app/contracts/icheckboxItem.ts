export interface ICheckboxItem {
  key: string;
  label: string;
  className: string;
  groupIndex: number;
}
