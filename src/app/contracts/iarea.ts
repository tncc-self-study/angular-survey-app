export interface IArea {
  label: string;
  value: string;
}
