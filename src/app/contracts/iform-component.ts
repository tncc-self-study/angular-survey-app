import { FormlyFieldConfig } from '@ngx-formly/core';

export interface IFormComponent {
  model: any;
  fields: FormlyFieldConfig[];
}
