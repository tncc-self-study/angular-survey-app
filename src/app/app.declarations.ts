import { AppComponent } from './app.component';
import { OrderComponent } from './components/order/order.component';
import { NampoComponent } from './components/nampo/nampo.component';
import { HomeComponent } from './components/home/home.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { FooterComponent } from './components/footer/footer.component';

const declarations = [
  AppComponent,
  OrderComponent,
  NampoComponent,
  HomeComponent,
  NavMenuComponent,
  FooterComponent
];

export const Declarations = [ declarations ];
