import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { JsonContentTypeInterceptor } from './services/data.service';

import { AreaService } from './services/area.service';
import { CustomerService } from './services/customer.service';
import { ProductService } from './services/product.service';
import { IndustryService } from './services/industry.service';
import { SalesRepsService } from './services/salesreps.service';
import { OtherInfoService } from './services/otherinfo.service';
import { NampoService } from './services/nampo.service';
import { FormService } from './services/form.service';
import { ValidatorsService } from './services/validators.service';
import { FieldGroupService } from './services/fieldgroup.service';
import { DataService } from './services/data.service';
import { ToasterService } from './services/toaster.service';
import { AuthService } from './services/auth.service';

import { Configuration } from './app.constants';

const providers = [
  AreaService,
  CustomerService,
  ProductService,
  IndustryService,
  SalesRepsService,
  OtherInfoService,
  NampoService,
  FormService,
  ValidatorsService,
  FieldGroupService,
  DataService,
  ToasterService,
  Configuration,
  AuthService,
  { provide: HTTP_INTERCEPTORS, useClass: JsonContentTypeInterceptor, multi: true  }
];

export const Providers = [ providers ];
