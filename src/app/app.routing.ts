import { Routes, RouterModule } from '@angular/router';

import { OrderComponent } from './components/order/order.component';
import { NampoComponent } from './components/nampo/nampo.component';
import { HomeComponent } from './components/home/home.component';
import { ErrorComponent } from './components/error/error.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'order', component: OrderComponent },
  { path: 'survey-nampo', component: NampoComponent },
  { path: 'not-found', component: ErrorComponent, data: { error: 'Page not found!' } },
  { path: 'forbidden', component: ErrorComponent, data: { error: 'Forbidden Access' } },
  { path: 'auth-failed', component: ErrorComponent, data: { error: 'Authentication failed!' } },
  { path: '**', redirectTo: 'home' }
];

export const routing = RouterModule.forRoot(appRoutes);
