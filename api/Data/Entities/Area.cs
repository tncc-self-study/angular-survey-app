using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;
using SurveyApi.Data.Infrastructure;

namespace SurveyApi.Data.Entities
{
    public class Area : IEntity
    {
        public Area()
        {
            Id = ObjectId.GenerateNewId().ToString();
        }

        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get ; set; }

        [Required]
        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("index")]
        public int Index { get;set; }
        
        #region Overrides

        public override string ToString()
        {
            return $"{Description}";
        }

        #endregion Overrides
    }
}