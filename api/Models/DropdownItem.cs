namespace SurveyApi.Models
{
    public class DropdownItem
    {
        public string Label { get;set; }
        public string Value { get;set; }
    }
}