namespace SurveyApi.Models
{
    public class Settings
    {
        public string ConnectionString;
        public string DatabaseName;
    }
}