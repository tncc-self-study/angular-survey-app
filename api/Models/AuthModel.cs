using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace SurveyApi.Models
{
    public class AuthModel
    {
      [DataMember(Name="username")]
      [Required]
       public string Username { get; set; }
      [DataMember(Name="password")]
      [Required]
      public string Password { get; set; }
    }
}
