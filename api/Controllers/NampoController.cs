using System;
using System.Threading.Tasks;
using api.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SurveyApi.Contracts;
using SurveyApi.Data.Infrastructure;

namespace SurveyApi.Controllers
{
    [Authorize]
    public class NampoController : Controller
    {
        ILogger<NampoController> _logger;
        IRepository _repository;

        public NampoController(ILogger<NampoController> logger,
                               IRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [Route("api/survey/save")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]NampoSurvey survey)
        {
            _logger.LogDebug($"Receive survey {nameof(NampoController)}");

            try {
              await _repository.InsertAsync(survey);
              return Ok();
            }
            catch (Exception ex) {
              _logger.LogDebug($"Failed to insert survey. {ex.Message}");
              return BadRequest();
            }
        }
    }
}
